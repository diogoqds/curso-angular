import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { Photo } from '../../photo/photo';

@Component({
  selector: 'ap-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnChanges {

  @Input() photos: Photo[] = [];
  rows = []

  constructor() { }

  ngOnInit() {
    this.rows = this.groupeColumns(this.photos)
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.photos) {
      this.rows = this.groupeColumns(this.photos)
    }
  }

  groupeColumns(photos: Photo[]) {
    const newPhotos = [];

    for(let i = 0; i < photos.length; i+= 3) {
      newPhotos.push(photos.slice(i, i+3));
    }
    return newPhotos;
  }

}
