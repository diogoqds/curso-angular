import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { lowerCaseValidator } from 'src/app/core/validators/lower-case.validator';
import { UserNotTakenValidatorService } from './user-not-taken.validator.service';
import { NewUser } from './new-user.interface';
import { SignUpService } from './signup.service';
import { Router } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/platform-detector/platform-detector.service';

@Component({
  templateUrl: './signup.component.html',
  providers: [
    UserNotTakenValidatorService
  ]
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  @ViewChild('emailInput') emailInput: ElementRef<HTMLElement>;

  constructor(private formBuilder: FormBuilder,
             private userNotTakenValidatorService: UserNotTakenValidatorService,
             private signUpService: SignUpService,
             private router: Router,
             private platformDetectorService: PlatformDetectorService) {}

  ngOnInit(): void {
    this.signUpForm = this.formBuilder.group({
      email: [
        '',
        [
          Validators.required,
          Validators.email
        ]
      ],
      fullName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(40)
        ]
      ],
      userName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
          // Validators.pattern(/^[a-z0-9_\-]+$/)
          lowerCaseValidator
        ],
        this.userNotTakenValidatorService.checkUserNameIsTaken()
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(18),
        ]
      ]
    });

    this.focusEmailInput();
  }

  signup() {
    const newUser = this.signUpForm.getRawValue() as NewUser;
    this.signUpService
      .signup(newUser)
      .subscribe(
        () => this.router.navigate(['']),
        err => {
          this.focusEmailInput();
          alert('Não foi possivel fazer o cadastro');
        }
      )
  }

  focusEmailInput() {
    this.platformDetectorService.isBrowserPlatform() &&
    this.emailInput.nativeElement.focus();
  }
}