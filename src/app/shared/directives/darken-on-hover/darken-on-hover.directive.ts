import { Directive, ElementRef, Renderer, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[apDarkenOnHover]'
})
export class DarkenOnHoverDirective {

  @Input() brightness: string = '70%';

  constructor(private el: ElementRef, private render: Renderer) {}

  @HostListener('mouseover')
  darkHoverOn() {
    this.render.setElementStyle(this.el.nativeElement, 'filter', `brightness(${this.brightness})`)
  }

  @HostListener('mouseleave')
  darkHoverOff() {
    this.render.setElementStyle(this.el.nativeElement, 'filter', 'brightness(100%)')
  }
}